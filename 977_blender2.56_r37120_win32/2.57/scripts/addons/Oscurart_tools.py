# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

bl_info = {
    "name": "Oscurart Tools",
    "author": "Eugenio Pignataro",
    "version": (1,7),
    "blender": (2, 5, 6),
    "api": 35115,
    "location": "View3D > Tools > Oscurart Tools",
    "description": "Suma funcionalidades de modelado y render setting",
    "warning": "",
    "wiki_url": "oscurart.blogspot.com",
    "tracker_url": "",
    "category": "Object"}



import bpy
import math
import sys


## CREA PANELES EN TOOLS

class panelTools(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_label = "Oscurart Tools"


    def draw(self, context):
        active_obj = context.active_object
        layout = self.layout
        rou=layout.row()
        col = layout.column(align=0)
        col.label("Meshes Tools:")           
        col.operator("object.selmen",icon="VERTEXSEL")  
        col.operator("object.no",icon="SNAP_NORMAL")               
        col.operator("object.rs",icon="UV_SYNC_SELECT") 
        col.operator("object.rsvg",icon="UV_SYNC_SELECT")        
        col.label("Shapes Tools:")
        col.operator("object.cg",icon="GROUP_VERTEX")
        col.operator("object.cs",icon="SHAPEKEY_DATA")
        col.operator("object.csl",icon="SETTINGS")         
        col.operator("object.clas",icon="SETTINGS")   
        col.label("File Tools:")        
        col.operator("object.savinc",icon="NEW")
        col.operator("object.ri",icon="IMAGE_COL")              
        col.label("Render Tools:")
        col.operator("ren.render",icon="RENDER_STILL") 
        col.operator("ren.crop",icon="RENDER_REGION")  
        col.prop(bpy.context.scene,"rcPARTS",text="Render Crops Parts")  
        col.label("Objects Tools:")
        col.operator("object.disobjmin",icon="OBJECT_DATAMODE")   
        col.operator("object.disobjmax",icon="OBJECT_DATAMODE") 
        col.operator("object.mmapply",icon="OBJECT_DATAMODE")                       
        col.prop(bpy.context.scene,"SearchSelect")
        col.operator("object.sas",icon="ZOOM_SELECTED")
        col.prop(bpy.context.scene,"RenameObject")
        col.operator("object.rnm",icon="SHORTDISPLAY") 
              

                     

        
        
##---------------------------RELOAD IMAGES------------------

class reloadImages (bpy.types.Operator):
    bl_idname = "object.ri"
    bl_label = "Reload Images" 
    def execute(self,context):
        for imgs in bpy.data.images:
            imgs.reload()
        return("FINISHED")

##-----------------------------RESYM---------------------------

class resym (bpy.types.Operator):
    bl_idname = "object.rs"
    bl_label = "ReSym Mesh" 
    def execute(self,context):
        ##SETEO VERTEX MODE        
        bpy.context.tool_settings.mesh_select_mode[0]=1
        bpy.context.tool_settings.mesh_select_mode[1]=0
        bpy.context.tool_settings.mesh_select_mode[2]=0
        
        OBJLIST = bpy.context.selected_objects
        
        for OBJETOS in OBJLIST:
            ## OBJETO ACTIVO
            bpy.data.scenes[0].objects.active = bpy.data.objects[OBJETOS.name]
            ##BORRA IZQUIERDA
            bpy.ops.object.mode_set(mode="EDIT", toggle=False)
            bpy.ops.mesh.select_all(action="DESELECT")
            bpy.ops.object.mode_set(mode="OBJECT", toggle=False)
            for vertices in bpy.data.objects[OBJETOS.name].data.vertices:
              if vertices.co[0] < 0:
                vertices.select = 1
            ## EDIT    
            bpy.ops.object.mode_set(mode="EDIT", toggle=False)
            ## BORRA COMPONENTES
            bpy.ops.mesh.delete()
            ## SUMA MIRROR
            bpy.ops.object.modifier_add(type='MIRROR')
            ## PASO A EDIT MODE
            bpy.ops.object.mode_set(mode="EDIT", toggle= False)
            ## SELECCIONO TODOS LOS COMPONENTES
            bpy.ops.mesh.select_all(action="SELECT")
            ## CREO UV TEXTURE DEL SIMETRICO
            bpy.ops.mesh.uv_texture_add()
            ## SETEO VARIABLE CON LA CANTIDAD DE UVS, RESTO UNO Y LE DOY UN NOMBRE
            LENUVLISTSIM = len(bpy.data.objects[OBJETOS.name].data.uv_textures)
            LENUVLISTSIM = LENUVLISTSIM - 1
            print ("LA CANTIDAD DE UVS QUE TIENE ES: "+ str(LENUVLISTSIM))
            bpy.data.objects[OBJETOS.name].data.uv_textures[LENUVLISTSIM:][0].name = "SIMETRICO"
            ## MODO EDICION
            bpy.ops.object.mode_set(mode="EDIT", toggle= False)
            ## UNWRAP
            bpy.ops.uv.unwrap(method='ANGLE_BASED', fill_holes=True, correct_aspect=True)
            ## MODO OBJETO
            bpy.ops.object.mode_set(mode="OBJECT", toggle= False) 
            ## APLICO MIRROR
            bpy.ops.object.modifier_apply(apply_as='DATA', modifier="Mirror")
            ## VUELVO A EDIT MODE
            bpy.ops.object.mode_set(mode="EDIT", toggle= False)
            ## CREO UV TEXTURE DEL ASIMETRICO
            bpy.ops.mesh.uv_texture_add()
            ## SETEO VARIABLE CON LA CANTIDAD DE UVS, RESTO UNO Y LE DOY UN NOMBRE
            LENUVLISTASIM = len(bpy.data.objects[OBJETOS.name].data.uv_textures)
            LENUVLISTASIM = LENUVLISTASIM  - 1
            print ("LA CANTIDAD DE UVS QUE TIENE ES: "+ str(LENUVLISTASIM))
            bpy.data.objects[OBJETOS.name].data.uv_textures[LENUVLISTASIM:][0].name = "ASIMETRICO"
            ## SETEO UV ACTIVO
            bpy.data.objects[OBJETOS.name].data.uv_textures.active = bpy.data.meshes[OBJETOS.data.name].uv_textures["ASIMETRICO"]
            ## EDIT MODE
            bpy.ops.object.mode_set(mode="EDIT", toggle= False)
            ## UNWRAP
            bpy.ops.uv.unwrap(method='ANGLE_BASED', fill_holes=True, correct_aspect=True)
            ## PASO A OBJECT MODE
            bpy.ops.object.mode_set(mode="OBJECT", toggle= False)
        return("FINISHED")     
        
## -----------------------------------SELECT LEFT---------------------
class SelectMenor (bpy.types.Operator):
    bl_idname = "object.selmen"
    bl_label = "Select Left" 
    def execute(self,context):  
        ## LEVANTO EL MODO ACTUAL
        VARMODE=0
        MODE=bpy.context.mode
        if MODE == "EDIT_MESH":
            bpy.ops.object.mode_set(mode="OBJECT", toggle=0)
            VARMODE=1  
        ##SETEO VERTEX MODE
        
        bpy.context.tool_settings.mesh_select_mode[0]=1
        bpy.context.tool_settings.mesh_select_mode[1]=0
        bpy.context.tool_settings.mesh_select_mode[2]=0
        
        ## DESELECCIONA TODO
        for object in bpy.context.selected_objects:
            print ("LA DATA DEL OBJETO SE LLAMA:  "+str(object.data.name))
            for vertices in bpy.data.meshes[object.data.name].vertices:
                vertices.select = 0
        
        ## CONDICION QUE SI EL VERTICE ES MENOR A 0 LO SELECCIONA
        for object in bpy.context.selected_objects:
                print (object.data.name)
                for vertices in bpy.data.meshes[object.data.name].vertices:
                    if vertices.co[0] < 0:
                        print ("es menor")
                        vertices.select = True    
        ## DEVUELO EL MODE                
        if VARMODE ==1:                
            bpy.ops.object.mode_set(mode="EDIT", toggle=0)         
        return("FINISHED")        
           
    
    
    

##-----------------------------------CREATE SHAPES----------------       
class CreaShapes(bpy.types.Operator):
    bl_idname = "object.cs"
    bl_label = "Split LR Shapes"
    def execute(self, context):
        OBJETOACTIVO = bpy.context.active_object.name
        SHAPEBASIS = bpy.context.active_object.data.shape_keys.key_blocks[0].name
        COLECCIONSHAPES = bpy.data.objects[OBJETOACTIVO].data.shape_keys.key_blocks
        
        for SHAPE in COLECCIONSHAPES:
            ## RESETEA VALORES
            bpy.ops.object.shape_key_clear()
            ## SHAPE ACTIVO AL VALOR DE 1
            SHAPE.value = 1
            ## DUPLICA Y NOMBRA LOS SHAPES COMO L Y R
            bpy.ops.object.shape_key_add(from_mix=True)
            bpy.data.objects[OBJETOACTIVO].data.shape_keys.key_blocks[-1].name = str(SHAPE.name)+"_L"
            bpy.ops.object.shape_key_add(from_mix=True)
            bpy.data.objects[OBJETOACTIVO].data.shape_keys.key_blocks[-1].name = str(SHAPE.name)+"_R"
        
        ## INDICA EN LA LISTA EL NUMERO DE SHAPE A OPERAR
        INDICESHAPES = 0
        
        COLECCIONSHAPES = bpy.data.objects[OBJETOACTIVO].data.shape_keys.key_blocks
        ## SETEO LOS SHAPES L Y R    
        for SHAPELR in bpy.data.objects[OBJETOACTIVO].data.shape_keys.key_blocks:
            ## SETEO EL INDICE
            bpy.data.objects[OBJETOACTIVO].active_shape_key_index = INDICESHAPES
            print ("EL INDICE DEL SHAPE A TRABAJAR ES"+ str(INDICESHAPES))
            ## SI EL SHAPE TERMINA EN ...
            if SHAPELR.name.endswith("_L") == True:
                print (str(SHAPELR.name)+" ES IGUAL A TRUE" )
                ## RESETEO SHAPES
                bpy.ops.object.shape_key_clear()
                ## PARA CADA VERTICE EN EL OBJETO SELECCIONO LOS MENORES A CERO Y MEZCLO
                bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = 0
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                bpy.ops.object.vertex_group_select()
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)               
                # MEZCLO
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                bpy.ops.mesh.blend_from_shape(shape=SHAPEBASIS, blend=1, add=False)
                bpy.ops.mesh.select_all(action="DESELECT")
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
                ## PARA CADA VERTICE EN EL OBJETO SELECCIONO LOS MENORES A CERO Y MEZCLO
                bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = 2
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                bpy.ops.object.vertex_group_select()
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
                # MEZCLO
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                bpy.ops.mesh.blend_from_shape(shape=SHAPEBASIS, blend=.5, add=False)
                bpy.ops.mesh.select_all(action="DESELECT")
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
            ## SI EL SHAPE TERMINA EN ...
            if SHAPELR.name.endswith("_R") == True:
                print (str(SHAPELR.name)+" ES IGUAL A TRUE" )
                ## RESETEO SHAPES
                bpy.ops.object.shape_key_clear()
                ## PARA CADA VERTICE EN EL OBJETO SELECCIONO LOS MENORES A CERO Y MEZCLO
                bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = 1
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                bpy.ops.object.vertex_group_select()
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)              
                # MEZCLO
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                bpy.ops.mesh.blend_from_shape(shape=SHAPEBASIS, blend=1, add=False)
                bpy.ops.mesh.select_all(action="DESELECT")
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
                ## PARA CADA VERTICE EN EL OBJETO SELECCIONO LOS MENORES A CERO Y MEZCLO
                bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = 2
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                bpy.ops.object.vertex_group_select()
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False) 
                # MEZCLO
                bpy.ops.object.mode_set(mode='EDIT', toggle=False)
                bpy.ops.mesh.blend_from_shape(shape=SHAPEBASIS, blend=.5, add=False)
                bpy.ops.mesh.select_all(action="DESELECT")
                bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
                        
                        
                                
            INDICESHAPES = INDICESHAPES + 1
                        
            
        bpy.ops.object.shape_key_clear()
        
        
        print ("OPERACION TERMINADA")        
        return("FINISHED")
    
    
##----------------------------SHAPES LAYOUT-----------------------

class CreaShapesLayout(bpy.types.Operator):
    bl_idname = "object.csl"
    bl_label = "Create Symmetrical Shapes Layout"
    def execute(self, context):
        
  
        SEL_OBJ= bpy.context.active_object
        LISTA_KEYS = bpy.context.active_object.data.shape_keys.key_blocks
        
        ##MODOS
        EDITMODE = "bpy.ops.object.mode_set(mode='EDIT')"
        OBJECTMODE = "bpy.ops.object.mode_set(mode='OBJECT')"
        POSEMODE = "bpy.ops.object.mode_set(mode='POSE')"
        
        ##INDICE DE DRIVERS
        varindex = 0
        
        ##CREA NOMBRES A LA ARMATURE
        amt = bpy.data.armatures.new("ArmatureData")
        ob = bpy.data.objects.new("RIG_LAYOUT_"+SEL_OBJ.name, amt)
        
        ##LINK A LA ESCENA
        scn = bpy.context.scene
        scn.objects.link(ob)
        scn.objects.active = ob
        ob.select = True
        
        
        
        
        
        eval(EDITMODE)
        gx = 0
        gy = 0
        
        
        
        for keyblock in LISTA_KEYS:
            print ("KEYBLOCK EN CREACION DE HUESOS "+keyblock.name)
        
                
            if keyblock.name[-2:] != "_L":
                if keyblock.name[-2:] != "_R":
        
                    ##CREA HUESOS
                
                    bone = amt.edit_bones.new(keyblock.name)
                    bone.head = (gx,0,0)
                    bone.tail = (gx,0,1)
                    gx = gx+2.2
                    bone = amt.edit_bones.new(keyblock.name+"_CTRL")
                    bone.head = (gy,0,0)
                    bone.tail = (gy,0,0.2)
                    gy = gy+2.2     
                  
                    ##SETEA ARMATURE ACTIVA
                    bpy.context.scene.objects.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].select = 1
                    ##DESELECCIONA (modo edit)
                    eval(EDITMODE)
                    bpy.ops.armature.select_all(action="DESELECT")
                    
                    ##EMPARENTA HUESOS
                
                    ##HUESO ACTIVO
                    eval(OBJECTMODE)
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[keyblock.name] 
                    ##MODO EDIT
                    eval(EDITMODE)       
                    ##DESELECCIONA (modo edit)
                    bpy.ops.armature.select_all(action="DESELECT")    
                    ##MODO OBJECT  
                    eval(OBJECTMODE)    
                    ##SELECCIONA UN HUESO
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[keyblock.name+"_CTRL"].select = 1
                    eval(EDITMODE)    
                    ##EMPARENTA
                    bpy.ops.armature.parent_set(type="OFFSET")
                    ##DESELECCIONA (modo edit)
                    bpy.ops.armature.select_all(action="DESELECT")      
                     
                    ##LE HAGO UNA VARIABLE DE PLACEBO
                    keyblock.driver_add("value") 
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.expression = "var+var_000"
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables.new()
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables.new()           
                      
                    
                    varindex = varindex + 1 
        
        
        
        for keyblock in LISTA_KEYS:
            print ("KEYBLOCK SEGUNDA VUELTA :"+keyblock.name)    
            
            if keyblock.name[-2:] == "_L":
                print("igual a L") 
        
                ##CREA DRIVERS Y LOS CONECTA
                keyblock.driver_add("value")
                keyblock.driver_add("value")
                
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.expression = "var+var_000"
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables.new()
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables.new()
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].id  = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name] 
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].bone_target   = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[(keyblock.name[:-2])+"_CTRL"].name
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].type = 'TRANSFORMS' 
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].use_local_space_transform = 1        
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].transform_type= "LOC_X"  
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].id = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].bone_target   = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[(keyblock.name[:-2])+"_CTRL"].name    
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].type = 'TRANSFORMS'            
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].use_local_space_transform = 1        
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].transform_type= "LOC_Y"    
                        
                             
                varindex = varindex + 1
            
                
        
          
            if keyblock.name[-2:] == "_R":
                print("igual a R") 
                
                ##CREA DRIVERS Y LOS CONECTA
                keyblock.driver_add("value")
                keyblock.driver_add("value")        
                
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.expression = "-var+var_000"
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables.new()
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables.new()
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].id  = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name] 
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].bone_target   = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[(keyblock.name[:-2])+"_CTRL"].name      
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].type = 'TRANSFORMS' 
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].use_local_space_transform = 1                 
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].transform_type= "LOC_X"  
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].id = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].bone_target   = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[(keyblock.name[:-2])+"_CTRL"].name    
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].type = 'TRANSFORMS'            
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].use_local_space_transform = 1        
                SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].transform_type= "LOC_Y" 
                        
                varindex = varindex + 1       
                
                
                
                
        ## CREO DATA PARA SLIDERS
        
        ## creo data para los contenedores
        verticess = [(-1,1,0),(1,1,0),(1,-1,0),(-1,-1,0)]
        edgess = [(0,1),(1,2),(2,3),(3,0)]
        
        mesh = bpy.data.meshes.new(keyblock.name+"_data_container")
        object = bpy.data.objects.new("GRAPHIC_CONTAINER", mesh)
        bpy.context.scene.objects.link(object)
        mesh.from_pydata(verticess,edgess,[])
        
        ## PONGO LOS LIMITES Y SETEO ICONOS
        for keyblock in LISTA_KEYS:
            print ("KEYBLOCK EN CREACION DE HUESOS "+keyblock.name)
        
                
            if keyblock.name[-2:] != "_L":
                if keyblock.name[-2:] != "_R":
                    ## SETEO ICONOS
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name].custom_shape = bpy.data.objects['GRAPHIC_CONTAINER']
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].custom_shape = bpy.data.objects['GRAPHIC_CONTAINER']
                    ## SETEO CONSTRAINTS
                    eval(OBJECTMODE)
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[keyblock.name+"_CTRL"]
                    ## SUMO CONSTRAINT
                    eval(POSEMODE)
                    bpy.ops.pose.constraint_add(type="LIMIT_LOCATION")
                    
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].min_x = -1
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_min_x = 1
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].min_z = 0
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_min_z = 1
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].min_y = -1
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_min_y = 1
                    
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].max_x =  1
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_max_x = 1
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].max_z =  0
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_max_z = 1
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].max_y =  1
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_max_y = 1
                    
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].owner_space  = "LOCAL"
                    
        ## PARA QUE EL TEXTO FUNCIONE PASAMOS A OBJECT MODE
        eval(OBJECTMODE)
        
        ## TEXTOS
        for keyblock in LISTA_KEYS:
            print ("KEYBLOCK EN TEXTOS "+keyblock.name)
        
                
            if keyblock.name[-2:] != "_L":
                if keyblock.name[-2:] != "_R":            
                    ## creo tipografias
                    bpy.ops.object.text_add(location=(0,0,0))
                    bpy.data.objects['Text'].data.body = keyblock.name
                    bpy.data.objects['Text'].name = "TEXTO_"+keyblock.name
                    bpy.data.objects["TEXTO_"+keyblock.name].rotation_euler[0] = math.pi/2
                    bpy.data.objects["TEXTO_"+keyblock.name].location.x = -1
                    bpy.data.objects["TEXTO_"+keyblock.name].location.z = -1
                    bpy.data.objects["TEXTO_"+keyblock.name].data.size = .2
                    ## SETEO OBJETO ACTIVO
                    bpy.context.scene.objects.active = bpy.data.objects["TEXTO_"+keyblock.name]
                    bpy.ops.object.constraint_add(type="COPY_LOCATION")
                    bpy.data.objects["TEXTO_"+keyblock.name].constraints['Copy Location'].target = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
                    bpy.data.objects["TEXTO_"+keyblock.name].constraints['Copy Location'].subtarget = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[keyblock.name].name
                    
                    bpy.ops.object.select_all(action="DESELECT")
                    bpy.data.objects["TEXTO_"+keyblock.name].select = 1 
                    bpy.context.scene.objects.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
                    bpy.ops.object.parent_set(type="OBJECT") 
                    
        
        ## EMPARENTA ICONO
        
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects["GRAPHIC_CONTAINER"].select = 1 
        bpy.context.scene.objects.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
        bpy.ops.object.parent_set(type="OBJECT")
        
        eval(POSEMODE)  
  
  
  
  
  
  
  
              
        return("FINISHED")    
        
##----------------------------CREATE LMR GROUPS-------------------
class CreaGrupos(bpy.types.Operator):
    bl_idname = "object.cg"
    bl_label = "Create LMR groups"
    def execute(self, context):
                ## SETEO VERTEX MODE EDIT
        bpy.context.window.screen.scene.tool_settings.mesh_select_mode[0] = 1
        bpy.context.window.screen.scene.tool_settings.mesh_select_mode[1] = 0
        bpy.context.window.screen.scene.tool_settings.mesh_select_mode[2] = 0
        
        print ("---------------- INICIA CREACION DE VERTEX GROUPS ------------")
        OBJETOACTIVO = bpy.context.active_object.name
        bpy.ops.object.mode_set(mode="EDIT", toggle=False)
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode="OBJECT")
        
        ## MENORES A CERO
        for vertices in bpy.data.objects[OBJETOACTIVO].data.vertices:
            if vertices.co[0] < -0.0001:
                vertices.select = 1
                
        bpy.ops.object.mode_set(mode="EDIT", toggle=False)
        bpy.ops.object.vertex_group_assign(new=1)
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode="OBJECT", toggle=False)
        bpy.data.objects[OBJETOACTIVO].vertex_groups[-1].name = "R" 
        
        ## MAYORES A CERO
        for vertices in bpy.data.objects[OBJETOACTIVO].data.vertices:
            if vertices.co[0] > 0.0001:
                vertices.select = 1
                
        bpy.ops.object.mode_set(mode="EDIT", toggle=False)
        bpy.ops.object.vertex_group_assign(new=1)
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode="OBJECT", toggle=False)
        bpy.data.objects[OBJETOACTIVO].vertex_groups[-1].name = "L" 
        
        ## IGUALES A CERO
        bpy.ops.object.mode_set(mode="EDIT", toggle=False)
        LARGOLISTAVG = len (bpy.data.objects[OBJETOACTIVO].vertex_groups)
        print ("EL LARGO DE LA LISTA ES: "+str(LARGOLISTAVG))
        bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = LARGOLISTAVG - 1
        bpy.ops.object.vertex_group_select()
        bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = LARGOLISTAVG - 2
        bpy.ops.object.vertex_group_select()
        bpy.ops.mesh.select_inverse()
        bpy.ops.object.vertex_group_assign(new=1)
        bpy.ops.mesh.select_all(action='DESELECT')
        bpy.ops.object.mode_set(mode="OBJECT", toggle=False)
        bpy.data.objects[OBJETOACTIVO].vertex_groups[-1].name = "M" 
        
        ## REORDENO LOS VERTEX GROUPS ARRIBA
        
        LENVG = len(bpy.data.objects[OBJETOACTIVO].vertex_groups)
        
        ULTIMO = LENVG-1
        ANTEULTIMO = -LENVG-2
        ANTEPENULTIMO = LENVG-3
        
        ## ACOMODO LA M
        bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = ULTIMO
        M = 1
        while M < LENVG:
            print(" M sube un nivel")
            bpy.ops.object.vertex_group_move(direction="UP")
            M=M+1
        
        ## ACOMODO LA L
        bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = ULTIMO
        L = 1
        while L < LENVG:
            print(" L sube un nivel")
            bpy.ops.object.vertex_group_move(direction="UP")
            L=L+1
            
        ## ACOMODO LA R
        bpy.data.objects[OBJETOACTIVO].vertex_groups.active_index = ULTIMO
        R = 1
        while R < LENVG:
            print(" R sube un nivel")
            bpy.ops.object.vertex_group_move(direction="UP")
            R=R+1
        
        print ("---------------- FINALIZA CREACION DE VERTEX GROUPS ------------")
        return("FINISHED")
    
##------------------------------NORMALS OUTSIDE--------------------    
class normalsOutside(bpy.types.Operator):  
    bl_idname = "object.no"
    bl_label = "Normals Outside"
    def execute(self, context):
        for OBJETO in bpy.context.selected_objects:
            ## SETEA OBJETO ACTIVO
            bpy.data.scenes[0].objects.active = bpy.data.objects[OBJETO.name]
            ## EDICION
            bpy.ops.object.mode_set(mode='EDIT', toggle=False)
            ## SELECCIONA TODOS LOS COMPONENTES 
            bpy.ops.mesh.select_all(action="SELECT")
            ## EXPULSA NORMALES
            bpy.ops.mesh.normals_make_consistent(inside=False)
            ## EDICION
            bpy.ops.object.mode_set(mode='OBJECT', toggle=False)
        return("FINISHED")


##------------------------------DISTRIBUTE---------------------------

 
## CREO PROPIEDADES DE NOMBRES
bpy.types.Scene.OscObjMin = bpy.props.StringProperty(default="Objeto menor")
bpy.types.Scene.OscObjMax = bpy.props.StringProperty(default="Objeto mayor")

def distributeObjects(AXIS):
    ## LISTA OBJETOS
    var = bpy.context.selected_objects
    
    ## AVERIGUO VALORES Y LOS AGREGO EN LA LISTA
    VAR=[]
    for objeto in var:
        VAR.append(objeto.location[AXIS])
        
    ## OREDEN LISTA    
    VAR.sort() 
    
    ## SETEO VARIABLES DE TAMANIO, MAXIMOS Y MINIMOS VALORES DE DISTANCIA
    
    LENVAR= len(VAR)
    INDEXVAR=0
    VARMIN= bpy.data.objects[bpy.context.scene.OscObjMin].location[AXIS]
    VARMAX= bpy.data.objects[bpy.context.scene.OscObjMax].location[AXIS]
    DIFESPACIO=VARMAX-VARMIN
    BETWEEN=DIFESPACIO/(LENVAR-1)
    
    ## APLICO VALORES
    for objeto in var:
        objeto.location[AXIS] = (BETWEEN*(INDEXVAR))+VARMIN
        INDEXVAR += 1        


class DisSelectMenor (bpy.types.Operator):
    bl_idname = "object.disobjmin"
    bl_label = "Set Min Object" 
    def execute(self,context): 
        bpy.context.scene.OscObjMin = bpy.context.active_object.name
        return("FINISHED")
    
class DisSelectMayor (bpy.types.Operator):
    bl_idname = "object.disobjmax"
    bl_label = "Set Max Object" 
    def execute(self,context): 
        bpy.context.scene.OscObjMax = bpy.context.active_object.name
        return("FINISHED")                
        

      
class DistributeMinMaxApply (bpy.types.Operator):
    bl_idname = "object.mmapply"
    bl_label = "Distribute MM Apply" 
    def execute(self,context): 
        
        ## EJECUTO        
        distributeObjects(0)
        distributeObjects(1)  
        distributeObjects(2) 
        return("FINISHED")            


##--------------------------------RENDER LAYER AT TIME----------------------------

class render (bpy.types.Operator):
    bl_idname="ren.render"
    bl_label="Render layers at time"
    def execute(self,context):
        CURSC= bpy.context.scene.name 
        PATH = bpy.data.scenes[CURSC].render.filepath
        ENDPATH = PATH
        FILEPATH=bpy.data.filepath
        if sys.platform.startswith("w"):
            print ("PLATFORM: WINDOWS")
            SCENENAME=(FILEPATH.rsplit("\\")[-1])[:-6]            
        else:
            print ("PLATFORM:LINUX")    
            SCENENAME=(FILEPATH.rsplit("/")[-1])[:-6]
        print (PATH)
        LAYERLIST=[]
        for layer in bpy.context.scene.render.layers:
            if layer.use == 1:
                LAYERLIST.append(layer)
            
        for layers in LAYERLIST:
            for rl in LAYERLIST:
                rl.use= 0
            print (layers.name)
            bpy.context.scene.render.filepath = PATH+"/"+SCENENAME+"/"+CURSC+"/"+layers.name+"/"+SCENENAME+"_"+layers.name
            escena = bpy.context.scene.name
            bpy.context.scene.render.layers[layers.name].use = 1
            bpy.ops.render.render(animation=1, layer=layers.name, scene= escena)
        print ("TERMINATED")
        
        ## REESTABLECE LOS LAYERS
        for layer in LAYERLIST:
            layer.use = 1
        
        ## RESTAURA EL PATH FINAL
        bpy.context.scene.render.filepath = ENDPATH
        return("FINISHED")
    
##--------------------------RENDER CROP----------------------
## SETEO EL STATUS DEL PANEL PARA EL IF 
bpy.types.Scene.RcropStatus = bpy.props.BoolProperty(default=0)

## CREO DATA PARA EL SLIDER
bpy.types.Scene.rcPARTS = bpy.props.IntProperty(default=0,min=2,max=50,step=1) 


class renderCrop (bpy.types.Operator):
    bl_idname="ren.crop"
    bl_label="Render Crop: Render!"
    def execute(self,context):
        
        ## NOMBRE DE LA ESCENA
        SCENENAME=(bpy.data.filepath.rsplit("/")[-1]).rsplit(".")[0]        
        
        ## CREA ARRAY
        PARTES=[]
        START=1
        PARTS=bpy.context.scene.rcPARTS
        PARTS=PARTS+1
        while START < PARTS:
            PARTES.append(START)
            START=START+1
        print(PARTES)
        

        
        ##SETEO VARIABLE PARA LA FUNCION DE RENDER
        NUMERODECORTE=1
        
        ##ESCENA ACTIVA
        SCACT = bpy.context.scene
        
        ## SETEO CROP
        bpy.data.scenes[SCACT.name].render.use_crop_to_border = 1
        bpy.data.scenes[SCACT.name].render.use_border = 1
        
        ##A VERIGUO RES EN Y
        RESY=bpy.data.scenes[SCACT.name].render.resolution_y
        
        ## AVERIGUO EL PATH DE LA ESCENA
        OUTPUTFILEPATH=bpy.data.scenes[SCACT.name].render.filepath
        bpy.context.scene.render.filepath = OUTPUTFILEPATH+bpy.context.scene.name
        

        
        ## CUANTAS PARTES HARA
        LENPARTES=len(PARTES)
        
        ## DIVIDE 1 SOBRE LA CANTIDAD DE PARTES
        DIVISOR=1/PARTES[LENPARTES-1]
        
        ## SETEA VARIABLE DEL MARCO MINIMO Y MAXIMO
        CMIN=0
        CMAX=DIVISOR
        
        ## REMUEVE EL ULTIMO OBJETO DEL ARRAY PARTES
        PARTESRESTADA = PARTES.pop(LENPARTES-1)
        
        ## SETEA EL MINIMO Y EL MAXIMO CON LOS VALORES DE ARRIBA
        bpy.data.scenes[SCACT.name].render.border_min_y = CMIN
        bpy.data.scenes[SCACT.name].render.border_max_y = CMAX

        ##AVERIGUO EL SISTEMA
        if sys.platform.startswith("w"):
            print ("PLATFORM: WINDOWS")
            VARSYSTEM= "/"          
        else:
            print ("PLATFORM:LINUX")    
            VARSYSTEM= "/"

        
        ##SETEA EL OUTPUT PARA LA PRIMERA PARTE
        OUTPUTFILEPATH+bpy.context.scene.name
        bpy.context.scene.render.filepath = OUTPUTFILEPATH+SCENENAME+VARSYSTEM+SCENENAME+"_PART"+str(PARTES[0])+"_"
        
        ##RENDER PRIMERA PARTE
        bpy.ops.render.render(animation=True)
        bpy.context.scene.render.filepath
        
        ##SUMO UN NUMERO AL CORTE
        NUMERODECORTE = NUMERODECORTE+1


      
        
        
        ## RENDER!
        for PARTE in PARTES:
            ## SUMA A LOS VALORES DEL CROP
            CMIN = CMIN + DIVISOR
            CMAX = CMAX + DIVISOR
            print ("EL CROP ES DE "+str(CMIN)+" A "+str(CMAX))
            ## SETEA BORDE
            bpy.data.scenes[SCACT.name].render.border_min_y = CMIN
            bpy.data.scenes[SCACT.name].render.border_max_y = CMAX
            ## SETEA EL OUTPUT
            bpy.context.scene.render.filepath = OUTPUTFILEPATH+SCENENAME+VARSYSTEM+SCENENAME+"_PART"+str(NUMERODECORTE)+"_"
            print ("EL OUTPUT DE LA FUNCION ES " +bpy.context.scene.render.filepath)
            ## PRINTEA EL NUMERO DE CORTE
            print (PARTE)
            ## RENDER
            bpy.ops.render.render(animation=True)
            ## SUMO NUMERO DE CORTE
            NUMERODECORTE = NUMERODECORTE+1    
        
        
        ## REESTABLEZCO EL FILEPATH
        bpy.context.scene.render.filepath = OUTPUTFILEPATH  
        
        
        print ("RENDER TERMINADO")
        


        
        return("FINISHED")
class panelRenderCropSlider(bpy.types.Panel):
    bl_space_type = 'VIEW_3D'
    bl_region_type = 'TOOLS'
    bl_label = "Render Crop Slider"
    def draw(self, context):
        active_obj = context.active_object
        layout = self.layout
        layout.prop(bpy.context.scene,'rcPARTS',text="PARTS")


    
##------------------------ SEARCH AND SELECT ------------------------

## SETEO VARIABLE DE ENTORNO
bpy.types.Scene.SearchSelect = bpy.props.StringProperty(default="Object name initials")


class SearchAndSelect(bpy.types.Operator):
    bl_idname = "object.sas"
    bl_label = "Search and select"
    def execute(self, context): 
        for objeto in bpy.context.scene.objects:
            variableNombre = bpy.context.scene.SearchSelect
            if objeto.name.startswith(variableNombre) == True :
                objeto.select = 1
                print("Selecciona:" + str(objeto.name))
        return("FINISHED")

##-------------------------RENAME OBJECTS----------------------------------    

## CREO VARIABLE
bpy.types.Scene.RenameObject = bpy.props.StringProperty(default="Type here")

class renameObjects (bpy.types.Operator):
    bl_idname = "object.rnm"
    bl_label = "Rename Objects" 
    def execute(self,context):

        ## LISTA
        listaObj = bpy.context.selected_objects
        
        
        
        for objeto in listaObj:
            print (objeto.name)
            objeto.name = bpy.context.scene.RenameObject
        return("FINISHED")



##-------------------------RESYM VG---------------------------------- 




class resymVertexGroups (bpy.types.Operator):
    bl_idname = "object.rsvg"
    bl_label = "Resym Vertex Weights" 
    def execute(self,context):
        
        OBACTIVO=bpy.context.active_object
        VGACTIVO=OBACTIVO.vertex_groups.active.index
        MENORESACERO=[]
        MAYORESACERO=[]
        MENORESACEROYSG=[]
        
        
        ## LISTA DE LOS VERTICES QUE ESTAN EN GRUPOS
        VERTICESENGRUPOS=[0]
        for vertice in OBACTIVO.data.vertices:
            if len(vertice.groups.items()) > 0:
                VERTICESENGRUPOS.append(vertice.index)  
                
                
                
        ## VERTICES MENORES A CERO
        for verticeindex in VERTICESENGRUPOS:
            for indices in OBACTIVO.data.vertices[verticeindex].groups:
                if indices.group == VGACTIVO:     
                    if bpy.context.active_object.data.vertices[verticeindex].co[0] < 0:
                        MENORESACERO.append(bpy.context.active_object.data.vertices[verticeindex].index)
                        
        ## VERTICES MENORES A CERO Y SIN GRUPO
        for vertice in OBACTIVO.data.vertices:
            if vertice.co[0] < 0:
                MENORESACEROYSG.append(vertice.index)               
                
        
        ## VERTICES MAYORES A CERO
        for verticeindex in VERTICESENGRUPOS:
            for indices in OBACTIVO.data.vertices[verticeindex].groups:
                if indices.group == VGACTIVO:     
                    if bpy.context.active_object.data.vertices[verticeindex].co[0] > 0:
                        MAYORESACERO.append(bpy.context.active_object.data.vertices[verticeindex].index)
        
        ## TE MUESTRA LAS LISTAS
        print("-------------VERTICES EN GRUPOS-----------")        
        print (VERTICESENGRUPOS)
        print("-------------MENORES A CERO-----------")
        print (MENORESACERO)
        print("-------------MENORES A CERO SIN GRUPO-----------")
        print (MENORESACEROYSG)
        print("-------------MAYORES A CERO-----------")
        print (MAYORESACERO)
        
        
        ## SETEA WORK INDEX      
        for vertices in MAYORESACERO:
            for indices in OBACTIVO.data.vertices[vertices].groups:
                if indices.group == VGACTIVO: 
                    WORKINDEX = indices.group  
        
        ## DESELECCIONO COMPONENTES
        bpy.ops.object.mode_set(mode="EDIT",toggle=0)
        bpy.ops.mesh.select_all(action="DESELECT")
        bpy.ops.object.mode_set(mode="OBJECT",toggle=0)
        
        
        ## SETEO GRUPO
        for verticemenor in MENORESACEROYSG:
            for verticemayor in MAYORESACERO:
                if OBACTIVO.data.vertices[verticemenor].co[0] == -OBACTIVO.data.vertices[verticemayor].co[0]:
                    if OBACTIVO.data.vertices[verticemenor].co[1] == OBACTIVO.data.vertices[verticemayor].co[1]:   
                        if OBACTIVO.data.vertices[verticemenor].co[2] == OBACTIVO.data.vertices[verticemayor].co[2]: 
                            OBACTIVO.data.vertices[verticemenor].select = 1    
        
        ## ASSIGNO AL GRUPO
        bpy.ops.object.mode_set(mode="EDIT",toggle=0)
        bpy.ops.object.vertex_group_assign(new=False)
        bpy.ops.mesh.select_all(action="DESELECT")
        bpy.ops.object.mode_set(mode="OBJECT",toggle=0)
        
        ## MODO PINTURA
        bpy.ops.object.mode_set(mode="WEIGHT_PAINT",toggle=0)
        
        
        ##--------->> VUELVO A CREAR GRUPOS YA QUE LOS INDICES CAMBIARON
        MENORESACERO=[]
        MAYORESACERO=[]
        
        
        ## LISTA DE LOS VERTICES QUE ESTAN EN GRUPOS
        VERTICESENGRUPOS=[0]
        for vertice in OBACTIVO.data.vertices:
            if len(vertice.groups.items()) > 0:
                VERTICESENGRUPOS.append(vertice.index)  
                
                
                
        ## VERTICES MENORES A CERO
        for verticeindex in VERTICESENGRUPOS:
            for indices in OBACTIVO.data.vertices[verticeindex].groups:
                if indices.group == VGACTIVO:     
                    if bpy.context.active_object.data.vertices[verticeindex].co[0] < 0:
                        MENORESACERO.append(bpy.context.active_object.data.vertices[verticeindex].index)
        
        
        
        
        ## VERTICES MAYORES A CERO
        for verticeindex in VERTICESENGRUPOS:
            for indices in OBACTIVO.data.vertices[verticeindex].groups:
                if indices.group == VGACTIVO:     
                    if bpy.context.active_object.data.vertices[verticeindex].co[0] > 0:
                        MAYORESACERO.append(bpy.context.active_object.data.vertices[verticeindex].index)
        
        
        ## SETEO WEIGHT     
        for verticemenor in MENORESACERO:
            for verticemayor in MAYORESACERO:
                if OBACTIVO.data.vertices[verticemenor].co[0] == -OBACTIVO.data.vertices[verticemayor].co[0]:
                    if OBACTIVO.data.vertices[verticemenor].co[1] == OBACTIVO.data.vertices[verticemayor].co[1]:   
                        if OBACTIVO.data.vertices[verticemenor].co[2] == OBACTIVO.data.vertices[verticemayor].co[2]: 
                            VARINMAY = 0
                            VARINMEN = 0
                            while  OBACTIVO.data.vertices[verticemayor].groups[VARINMAY].group != VGACTIVO:
                                VARINMAY = VARINMAY+1
                            while  OBACTIVO.data.vertices[verticemenor].groups[VARINMEN].group != VGACTIVO:
                                VARINMEN = VARINMEN+1
                            ##print("Varinmay: "+str(VARINMAY)+" .Varinmen "+str(VARINMEN))  
                            OBACTIVO.data.vertices[verticemenor].groups[VARINMEN].weight = OBACTIVO.data.vertices[verticemayor].groups[VARINMAY].weight
                            
        
                          
        print("===============(TERMINADO)=============")       
        return("FINISHED")



##------------------------ SHAPES LAYOUT SYMMETRICA ------------------------

## SETEO VARIABLE DE ENTORNO
bpy.types.Scene.SearchSelect = bpy.props.StringProperty(default="Object name initials")


class CreateLayoutAsymmetrical(bpy.types.Operator):
    bl_idname = "object.clas"
    bl_label = "Create  Asymmetrical Shapes Layout"
    def execute(self, context): 
                
        SEL_OBJ= bpy.context.active_object
        LISTA_KEYS = bpy.context.active_object.data.shape_keys.key_blocks
        
        ##MODOS
        EDITMODE = "bpy.ops.object.mode_set(mode='EDIT')"
        OBJECTMODE = "bpy.ops.object.mode_set(mode='OBJECT')"
        POSEMODE = "bpy.ops.object.mode_set(mode='POSE')"
        
        ##INDICE DE DRIVERS
        varindex = 0
        
        ##CREA NOMBRES A LA ARMATURE
        amtas = bpy.data.armatures.new("ArmatureData")
        obas = bpy.data.objects.new("RIG_LAYOUT_"+SEL_OBJ.name, amtas)
        
        ##LINK A LA ESCENA
        scn = bpy.context.scene
        scn.objects.link(obas)
        scn.objects.active = obas
        obas.select = True
        
        
        
        
        
        eval(EDITMODE)
        gx = 0
        gy = 0
        
        
        
        for keyblock in LISTA_KEYS:
            print ("KEYBLOCK EN CREACION DE HUESOS "+keyblock.name)
        
                
            if keyblock.name[-2:] != "_L":
                if keyblock.name[-2:] != "_R":
        
                    ##CREA HUESOS
                
                    bone = amtas.edit_bones.new(keyblock.name)
                    bone.head = (gx,0,0)
                    bone.tail = (gx,0,1)
                    gx = gx+2.2
                    bone = amtas.edit_bones.new(keyblock.name+"_CTRL")
                    bone.head = (gy,0,0)
                    bone.tail = (gy,0,0.2)
                    gy = gy+2.2     
                  
                    ##SETEA ARMATURE ACTIVA
                    bpy.context.scene.objects.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].select = 1
                    ##DESELECCIONA (modo edit)
                    eval(EDITMODE)
                    bpy.ops.armature.select_all(action="DESELECT")
                    
                    ##EMPARENTA HUESOS
                
                    ##HUESO ACTIVO
                    eval(OBJECTMODE)
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[keyblock.name] 
                    ##MODO EDIT
                    eval(EDITMODE)       
                    ##DESELECCIONA (modo edit)
                    bpy.ops.armature.select_all(action="DESELECT")    
                    ##MODO OBJECT  
                    eval(OBJECTMODE)    
                    ##SELECCIONA UN HUESO
                    bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[keyblock.name+"_CTRL"].select = 1
                    eval(EDITMODE)    
                    ##EMPARENTA
                    bpy.ops.armature.parent_set(type="OFFSET")
                    ##DESELECCIONA (modo edit)
                    bpy.ops.armature.select_all(action="DESELECT")      
                     
                    ##CREA DRIVERS Y LOS CONECTA
                    keyblock.driver_add("value")
                    keyblock.driver_add("value")        
                    
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.expression = "-var+var_000"
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables.new()
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables.new()
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].id  = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name] 
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].bone_target   = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[(keyblock.name)+"_CTRL"].name      
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].type = 'TRANSFORMS' 
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].use_local_space_transform = 1                 
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var'].targets[0].transform_type= "LOC_X"  
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].id = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].bone_target   = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[(keyblock.name)+"_CTRL"].name    
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].type = 'TRANSFORMS'            
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].use_local_space_transform = 1        
                    SEL_OBJ.data.shape_keys.animation_data.drivers[varindex].driver.variables['var_000'].targets[0].transform_type= "LOC_Y" 
                          
                    
                    varindex = varindex + 1 
        
        
        
             
                
                
                
                
        ## CREO DATA PARA SLIDERS
        
        ## creo data para los contenedores
        verticess = [(-.1,1,0),(.1,1,0),(.1,0,0),(-.1,0,0)]
        edgess = [(0,1),(1,2),(2,3),(3,0)]
        
        mesh = bpy.data.meshes.new(keyblock.name+"_data_container")
        object = bpy.data.objects.new("GRAPHIC_CONTAINER_AS", mesh)
        bpy.context.scene.objects.link(object)
        mesh.from_pydata(verticess,edgess,[])
        
        ## PONGO LOS LIMITES Y SETEO ICONOS
        for keyblock in LISTA_KEYS:
            print ("KEYBLOCK EN CREACION DE HUESOS "+keyblock.name)
            ## SETEO ICONOS
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name].custom_shape = bpy.data.objects['GRAPHIC_CONTAINER_AS']
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].custom_shape = bpy.data.objects['GRAPHIC_CONTAINER_AS']
            ## SETEO CONSTRAINTS
            eval(OBJECTMODE)
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[keyblock.name+"_CTRL"]
            ## SUMO CONSTRAINT
            eval(POSEMODE)
            bpy.ops.pose.constraint_add(type="LIMIT_LOCATION")
            
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].min_x = 0
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_min_x = 1
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].min_z = 0
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_min_z = 1
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].min_y = 0
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_min_y = 1
            
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].max_x =  0
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_max_x = 1
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].max_z =  0
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_max_z = 1
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].max_y =  1
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].use_max_y = 1
            
            bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].pose.bones[keyblock.name+"_CTRL"].constraints['Limit Location'].owner_space  = "LOCAL"
                    
        ## PARA QUE EL TEXTO FUNCIONE PASAMOS A OBJECT MODE
        eval(OBJECTMODE)
        
        ## TEXTOS
        for keyblock in LISTA_KEYS:
            print ("KEYBLOCK EN TEXTOS "+keyblock.name)            
            ## creo tipografias
            bpy.ops.object.text_add(location=(0,0,0))
            bpy.data.objects['Text'].data.body = keyblock.name
            bpy.data.objects['Text'].name = "TEXTO_"+keyblock.name
            bpy.data.objects["TEXTO_"+keyblock.name].rotation_euler[0] = math.pi/2
            bpy.data.objects["TEXTO_"+keyblock.name].location.x = -1
            bpy.data.objects["TEXTO_"+keyblock.name].location.z = -1
            bpy.data.objects["TEXTO_"+keyblock.name].data.size = .2
            ## SETEO OBJETO ACTIVO
            bpy.context.scene.objects.active = bpy.data.objects["TEXTO_"+keyblock.name]
            bpy.ops.object.constraint_add(type="COPY_LOCATION")
            bpy.data.objects["TEXTO_"+keyblock.name].constraints['Copy Location'].target = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
            bpy.data.objects["TEXTO_"+keyblock.name].constraints['Copy Location'].subtarget = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name].data.bones[keyblock.name].name
            
            bpy.ops.object.select_all(action="DESELECT")
            bpy.data.objects["TEXTO_"+keyblock.name].select = 1 
            bpy.context.scene.objects.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
            bpy.ops.object.parent_set(type="OBJECT") 
            
        
        ## EMPARENTA ICONO
        
        bpy.ops.object.select_all(action="DESELECT")
        bpy.data.objects["GRAPHIC_CONTAINER_AS"].select = 1 
        bpy.context.scene.objects.active = bpy.data.objects["RIG_LAYOUT_"+SEL_OBJ.name]
        bpy.ops.object.parent_set(type="OBJECT")
        
        eval(POSEMODE)  
                  
        

        return("FINISHED")


##------------------------ SHAPES LAYOUT SYMMETRICA ------------------------

class saveIncremental(bpy.types.Operator):
    bl_idname = "object.savinc"
    bl_label = "Save Incremental File"
    def execute(self, context):     
        ##SETEO VARIABLES
        filepath=bpy.data.filepath
        
        ##SI LA RUTA CONTIENE _V
        if filepath.count("_v") == 0:
            print("La escena no tiene numero")
            stpath=filepath.rsplit(".blend")
            incrementalValue=1
            print("El output es: "+ stpath[0]+"_v0"+str(incrementalValue)+".blend")
            output=stpath[0]+"_v0"+str(incrementalValue)+".blend"
            bpy.ops.wm.save_as_mainfile(filepath=output)  
            
            
        else:    
            sfilepath=filepath.split("_v")[0]
            idfilepath=(filepath.split("_v")[1])[:-6]
            stpath=sfilepath+"_v"
            incrementalValue=int(idfilepath)    
            
            if len(idfilepath) > 1 :
                if idfilepath[0] == "0":
                    print("El primer valor es cero")
                    incrementalValue+=1
                    print("El output es: "+ sfilepath+"_v0"+str(incrementalValue)+".blend")
                    output=sfilepath+"_v0"+str(incrementalValue)+".blend"
                    bpy.ops.wm.save_as_mainfile(filepath=output)
                else:
                    print("El primer valor no es cero")
                    incrementalValue+=1
                    print("El output es: "+ sfilepath+"_v"+str(incrementalValue)+".blend")  
                    output=sfilepath+"_v0"+str(incrementalValue)+".blend"
                    bpy.ops.wm.save_as_mainfile(filepath=output)              
            
            if len(idfilepath) <= 1 :
                print("No tiene primer valor")  
                incrementalValue+=1
                print("El output es: "+ sfilepath+"_v0"+str(incrementalValue)+".blend")  
                output=sfilepath+"_v0"+str(incrementalValue)+".blend"
                bpy.ops.wm.save_as_mainfile(filepath=output)      
        return("FINISHED")    
##======================================================================================FIN DE SCRIPTS    
    
    
def register():
    pass

def unregister():
    pass

if __name__ == "__main__":
    register()



## REGISTRA CLASSES
bpy.utils.register_class(SelectMenor)
bpy.utils.register_class(panelTools)
bpy.utils.register_class(CreaGrupos)
bpy.utils.register_class(CreaShapes)
bpy.utils.register_class(normalsOutside)
bpy.utils.register_class(resym)
bpy.utils.register_class(reloadImages)
bpy.utils.register_class(render)
bpy.utils.register_class(renderCrop)
bpy.utils.register_class(SearchAndSelect)
bpy.utils.register_class(CreaShapesLayout)
bpy.utils.register_class(renameObjects)
bpy.utils.register_class(resymVertexGroups)
bpy.utils.register_class(CreateLayoutAsymmetrical)
bpy.utils.register_class(DisSelectMenor)
bpy.utils.register_class(DisSelectMayor)
bpy.utils.register_class(DistributeMinMaxApply)
bpy.utils.register_class(saveIncremental)
