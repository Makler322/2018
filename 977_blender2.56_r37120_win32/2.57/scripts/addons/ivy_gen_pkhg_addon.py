bl_info = {
    "name": "Ivy Gen",
    "author": "testscreenings, PKHG",
    "version": (0,1),
    "blender": (2, 5, 7),
    "api": 35853,
    "location": "View3D > Add > Curve",
    "description": "Adds a generated ivy type",
    "warning": "Experimental",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Add Curve"}
    

import bpy
from bpy.props import *
import mathutils
import random
#from ivy_functions import *
import math

def adhesion2(old_pos, ob, data):
    targetBoundingSphere = data[0]
    minimum_dist = targetBoundingSphere[1] * bpy.context.scene.IvyMaxAdhesionDist
    distances = []

    adhesionVector = mathutils.Vector((0,0,0))
#    print('L14 minimum_dist ', minimum_dist)
    for i, face in enumerate(ob.data.faces):
        n = face.normal
        c = face.center + ob.location

        plane_dist = (n.dot(c - old_pos))
#        print('L20 plane_dist', plane_dist)
    # up to here this works
    # plane_dist is the distance to the plane defined by the face.center and face.normal
        pointOnPlane = old_pos - n * plane_dist
        #print('pointOnPlane', pointOnPlane)
        # now find the closest distance to the poly
        # test if pointOnPlane is already inside the poligon
        # else get closest point on the closest edge
        
        if plane_dist < minimum_dist:
            if isInside(ob, face, pointOnPlane):
                #print('isInside')
                #print('plane_dist', plane_dist)
                if not distances:
                    distances.append(positive(plane_dist))
                #print('distances [0]', distances[0])
                if plane_dist < distances[0]:
                    distances.append(positive(plane_dist))
                    distances.sort()
                    #print('shortest', distances[0])
                    adhesionVector = mathutils.Vector((old_pos - pointOnPlane)).normalize()
                    
            #else:
                #print('is Outside')
#    print('L44 distance', distances[0])
    print('\nL45 ADHESION', adhesionVector)
    return adhesionVector

def meshFromPoints(verts):
    scene = bpy.context.scene
    
    DebugMesh = bpy.data.meshes.new('pointCloud')
    DebugMesh.from_pydata(verts, [], [])
    DebugMesh.update()
    ob_new = bpy.data.objects.new('DEBUG', DebugMesh)
    scene.objects.link(ob_new)
    ob_new.select = True


def positive(val):
    if val < 0:
        val = -val
    return val

def isInside(ob, face, point):
    #tests if the point is always on the same side of the edges
    #seems to work reasonably well, except:
    #returns False if the point lies directly on a vert
    
    inside = False
    bigger = 0
    smaller = 0
    for edge in face.edge_keys:
        v1 = ob.data.verts[edge[0]]
        v2 = ob.data.verts[edge[1]]
        val = (v1.co - v2.co).cross(point - v1.co)
        com = (v1.co - v2.co).cross(point - ob.data.verts[edge[0]-2].co)
        prod = val.dot(com)
        if prod >= 0:
            bigger += 1
        else:
            smaller += 1

    if bigger == 0 or smaller == 0:
        return True
    else:
        return False

def vertsToArray(root):
    splineVerts = []
    for ivyNode in root.ivyNodes:
#        splineVerts += ivyNode.position.copy().resize4D().to_tuple() #pkhg gives a nonttype
         tmp = ivyNode.position.copy()
         tmp.resize_4d()
         tmp = tmp.to_tuple()
         #print("dbg L95 tmp adn splineVerts",tmp,splineVerts)
         splineVerts += tmp
    return splineVerts

def createIvyCurves(context, IVY):
    bpy.ops.object.select_all(action='DESELECT')
    scene = context.scene
    curve = bpy.data.curves.new("IVY", type = 'CURVE')

    for root in IVY.ivyRoots:
#        print('L100 generate Root with', len(root.ivyNodes), 'verts')
        newSpline = curve.splines.new(type = 'POLY')
        splineVerts = vertsToArray(root)
#        print('L103 splineVerts', splineVerts, len(splineVerts))
        newSpline.points.add(int(len(splineVerts)*0.25 - 1))
        newSpline.points.foreach_set('co', splineVerts)
        newSpline.order_u = 2
#pkhg        print("L112 dir newSpline",dir(newSpline))
        newSpline.use_endpoint_u = True

    newCurve = bpy.data.objects.new("IVY_Curve", curve)
    scene.objects.link(newCurve)
    newCurve.select = True
    scene.objects.active = newCurve
        
def computeBoundingSphere(objs):
    #needs improvement not really a bounding sphere because of use of
    #corners of boundboxes --> computes faster
    bb_verts = []
    for obj in objs:
        for bb_v in obj.bound_box:
            bb_verts.append(mathutils.Vector((bb_v[0],bb_v[1],bb_v[2]))*obj.matrix_world)
    bb_size = mathutils.Vector((0,0,0))
    for bb_v in bb_verts:
        bb_size += bb_v
    center = bb_size / len(bb_verts)
    radius = 0
    for bb_v in bb_verts:
        if bb_v.length > radius:
            radius = bb_v.length
    return [center, radius]

class IvyNode(): #IvyNode Baseclass
    '''
    def __init__(self, climbing, length, floatingLength):
        self.climbing = False
        self.length = 0.0
        self.floatingLength = 0.0
    '''
    
    # node position
    position = mathutils.Vector((0,0,0))
    
    # primary grow direction, a weighted sum of the previous directions
    primaryVector = (0,0,1)
    
    #adhesion vector as a result from other scene objects
    adhesionVector = (0,0,0)
    
    # a smoothed adhesion vector computed and used during the birth phase,
    #  since the ivy leaves are align by the adhesion vector, this smoothed vector
    #  allows for smooth transitions of leaf alignment
    smootAdhesionVector = (0,0,0)
    
    # length of the associated ivy branch at this node
    length = 0.0
    
    # length at the last node that was climbing
    floatingLength = 0.0
    
    # climbing state
    climbing = False


class IvyRoot(IvyNode):
    # List with containing IvyNodes
    def __init__(self):
        self.ivyNodes = []

    ivyNodes = []
    
    # living state of this Root
    alive = True
    
    # number of parents, represents the level in the root hierarchy
    parents = 0


class Ivy():
    # list of containing Roots
    def __init__(self):
        self.ivyRoots = []

    ivyRoots = []
    
    # the ivy size factor, influences the grow behaviour [0..0,1]
    ivySize = 1.0
    
    # leaf size factor [0..0,1]
    leafSize = 1.0
    
    # branch size factor [0..0,1]
    branchSize = 1.0
    
    # maximum length of an ivy branch segment that is freely floating [0..1]
    maxFloatingLength = 8

    # maximum length of an ivy branch segment that is freely floating [0..1]
    maxLength = 12

    # maximum distance for adhesion of scene object [0..1]
    maxAdhesionDistance = 0.1

    # the probability of producing a new ivy root per iteration [0..1]
    branchingProbability = 0.95
    
    # the probability of creating a new ivy leaf [0..1]
    leafingProbability = 0.7


    ######### SEEDING ROOTS ###########
    def seed(self, startLocation, parentOrder=0, primaryVector=mathutils.Vector((0,0,1))):
        tmpRoot = IvyRoot()             # create a new IvyRoot
        tmpRoot.parents = parentOrder   # set childstatus

        tmpIvy = IvyNode()              # new IvyNode
        tmpIvy.position = startLocation # position of IvyNode
        tmpIvy.primaryVector = primaryVector
        tmpIvy.length = 0.0001          # avoid division by zero

        tmpRoot.ivyNodes.append(tmpIvy) # put IvyNode int RootNode
        self.ivyRoots.append(tmpRoot)   # put RootNode into Ivy



    ######### one GROW iteration ######
    def grow(self, data, ob):
        
        # newRoots = []
        rootNr = 0
        for root in self.ivyRoots:
            #print('\nL230 rootNr', rootNr, 'parents', root.parents)
            rootNr += 1
            if root.alive == False:
                continue

            prevIvy = root.ivyNodes[-1]
#            print('L236 rootstart ', root.ivyNodes[0].position)
            # RANDOM VECTOR
            
            # check if it dies of to much floating
            if prevIvy.floatingLength > self.maxFloatingLength:
                root.alive = False
                continue
            
            # check if it dies of old age
            if prevIvy.length > self.maxLength:
                root.alive = False
                continue

            randomVector = (mathutils.Vector((random.random()-0.5,
                                              random.random()-0.5,
                                              random.random()-0.5)) +
                            mathutils.Vector((0,0,0.1)))
            
            # PRIMARY VECTOR
            primaryVector = prevIvy.primaryVector

            # ADHESION VECTOR
            #adhesionVector = adhesion2(prevIvy.position, self.ivyCollider, data)
            adhesionVector = mathutils.Vector((0,0,0))
            
            adhesionVector = adhesion3(prevIvy.position, ob.data, data)
            adhesionVector.normalize() 
            adhesionVector = -0.5 * adhesionVector 
            adhesionVector *= 0.1 / adhesionVector.length
            # GROW VECTOR
            growVector = bpy.context.scene.IvyGlobScale * ( bpy.context.scene.IvyRandomWeight * randomVector +
                            bpy.context.scene.IvyAdhesionWeight * adhesionVector)#pkhg?? +
#???pkhg                            bpy.context.scene.IvyPrimaryWeight * primaryVector )  

            # GRAVITY VECTOR
            gravityVector = mathutils.Vector((0,0,-1)) * bpy.context.scene.IvyGravityWeight
            gravityVector *= math.pow(prevIvy.floatingLength / self.maxFloatingLength, 0.7)
            
            # compute COLLISION
            ####################################################################################################
            ####################################################################################################
            ####################################################################################################
            ####### put in here for testing, until the error is fixed ##########################################
            climbing, new_pos = collision(ob, root, prevIvy.position + growVector + gravityVector)
            ####### delete, comment out when fixed #############################################################
            ####################################################################################################
            ####################################################################################################
            ####################################################################################################
            #print('\nL284 old_grow', growVector)
            try:
                growVector = new_pos - prevIvy.position - gravityVector
            except:
                pass
#            print('L289 new_grow', growVector)


            # create new IvyNode
            tmpIvy = IvyNode()

            # set climbing state
            try:
                tmpIvy.climbing = climbing
            except:
                pass

            # set position
            tmpIvy.position = prevIvy.position + growVector + gravityVector
#            print('L303 tmpIvy.position', tmpIvy.position)

            # set new PRIMARY VECTOR
            tmpIvy.primaryVector = prevIvy.primaryVector + tmpIvy.position - prevIvy.position
            tmpIvy.primaryVector.normalize()

            # length to this IvyNode from start of root
            tmpIvy.length = prevIvy.length + mathutils.Vector((tmpIvy.position - prevIvy.position)).length

            # floating length
            if prevIvy.climbing == False:
                tmpIvy.floatingLength = (prevIvy.floatingLength +
                                        (tmpIvy.position - prevIvy.position).length)
            
            # put IvyNode into ROOT
            root.ivyNodes.append(tmpIvy)


            # test for branching
            # if branching: self.seed(startLocation = tmpIvy.position, parentOrder = root.parentOrder + 1)
            #print("L323 len root.ivynodes",len(root.ivyNodes))
            
            # test for maximum rootdepth and death
            if root.parents > 3 or root.alive == False:
                continue
                
            # weight depending on ratio of node length to total length
            weight = 1.0 - (math.cos(tmpIvy.length / prevIvy.length * 2.0 * 0.5 + 0.5))
            weight = 1.1
            probability = random.random()
            #print('probability', probability * weight)
            if (probability * weight > self.branchingProbability):
                #print('seeding at', tmpIvy.position)
                
                self.seed(startLocation = tmpIvy.position,
                            parentOrder = root.parents +1,
                            primaryVector = tmpIvy.primaryVector)
                #self.ivyRoots[-1].ivyNodes[0].position = tmpIvy.position
                            
                #print('seeded at:', self.ivyRoots[-1].ivyNodes[0].position)
                #print('newRootsLength', len(self.ivyRoots[-1].ivyNodes))
                pass # seed new root

def generateSamples(faces, mesh):
    samplePoints = []
    rows = 3
    for face in faces:
#        print('L350 face', face.index)

        # adding the vertcoordinates --> doesn't account for shared verts
        samplePoints += ([mesh.vertices[i].co.copy() for i in face.vertices])
    
        isQuad = True

        # set for triangle
        if len(face.vertices) == 3:
            isQuad = False

        #the verts
        vert0 = mesh.vertices[face.vertices[0]].co
        vert1 = mesh.vertices[face.vertices[1]].co
        vert2 = mesh.vertices[face.vertices[2]].co
        if isQuad:
            vert3 = mesh.vertices[face.vertices[3]].co

        #the directionvectors of the face
        V0 = mesh.vertices[face.vertices[0]].co - mesh.vertices[face.vertices[1]].co
        V1 = mesh.vertices[face.vertices[1]].co - mesh.vertices[face.vertices[2]].co
        V2 = mesh.vertices[face.vertices[2]].co - mesh.vertices[face.vertices[0]].co
        if isQuad:
            V2 = mesh.vertices[face.vertices[2]].co - mesh.vertices[face.vertices[3]].co
            V3 = mesh.vertices[face.vertices[3]].co - mesh.vertices[face.vertices[0]].co

        # add edgesamples (3 each)
        a = 0.0
        samp = rows
        for i in range(samp):
            a += 1 / (samp + 1)
#            print('L381 edge a', a)
#            print('L382 i', i, 'a', a)
            esample0 = vert0 - V0 * a
            esample1 = vert1 - V1 * a
            esample2 = vert2 - V2 * a
            if isQuad:
                esample3 = vert3 - V3 * a
            samplePoints.append(esample0)
            samplePoints.append(esample1)
            samplePoints.append(esample2)
            if isQuad:
                samplePoints.append(esample3)



        # add facesamples (12)
        # maybe add a test here if the face is facing the point

        # get the two triangles with the smallest angle if face is quad

        angle0 = (V0).angle(-V1)
#        print('L402 vert 0',angle0)

        angle1 = (V1).angle(-V2)
        #print('vert 1',angle1)
        if isQuad:
            angle2 = (V2).angle(-V3)
            #print('vert 2',angle2)
    
            angle3 = (V3).angle(-V0)
            #print('vert 3',angle3)


        angle = angle0
        tri_1 = [[V0, -V1],[mesh.vertices[face.vertices[0]].co,
                            mesh.vertices[face.vertices[1]].co,
                            mesh.vertices[face.vertices[2]].co]]
        if isQuad:
            tri_2 = [[V2, -V3],[mesh.vertices[face.vertices[2]].co,
                                mesh.vertices[face.vertices[3]].co,
                                mesh.vertices[face.vertices[0]].co]]

            if angle1 < angle:
                angle = angle1
                tri_1 = [[V1, -V2],[mesh.vertices[face.vertices[1]].co,
                                    mesh.vertices[face.vertices[2]].co,
                                    mesh.vertices[face.vertices[3]].co]]
                tri_2 = [[V3, -V0],[mesh.vertices[face.vertices[3]].co,
                                    mesh.vertices[face.vertices[0]].co,
                                    mesh.vertices[face.vertices[1]].co]]
    
            if angle2 < angle:
                angle = angle2
                tri_1 = [[V2, -V3],[mesh.vertices[face.vertices[2]].co,
                                    mesh.vertices[face.vertices[3]].co,
                                    mesh.vertices[face.vertices[0]].co]]
                tri_2 = [[V0, -V1],[mesh.vertices[face.vertices[0]].co,
                                    mesh.vertices[face.vertices[1]].co,
                                    mesh.vertices[face.vertices[2]].co]]
    
            if angle3 < angle:
                angle = angle3
                tri_1 = [[V3, -V0],[mesh.vertices[face.vertices[3]].co,
                                    mesh.vertices[face.vertices[0]].co,
                                    mesh.vertices[face.vertices[1]].co]]
                tri_2 = [[V1, -V2],[mesh.vertices[face.vertices[1]].co,
                                    mesh.vertices[face.vertices[2]].co,
                                    mesh.vertices[face.vertices[3]].co]]

        #add samples from first tri
        samplePoints += samplesOnTry(tri_1[0][0], tri_1[0][1], tri_1[1], rows=rows)

        #add samples from second tri
        if isQuad:
            samplePoints += samplesOnTry(tri_2[0][0], tri_2[0][1], tri_2[1], rows=rows)

    #print('samplePoints - all: ', len(samplePoints))
    return samplePoints

def samplesOnTry(vecA, vecB, verts, rows=6):
    samples = []
    base = verts[1]
    #rows = 3

    b = 0.0
    for row in range(rows+1):
        b += 1 / (rows +1)
        a = 0.0
        c = 0.95
        samp = row

        for s in range(samp):
            a += 1 / (samp + 1)
            sample = (base + (vecA.lerp(vecB, a) * b * c))     #concerning b: still need to offset for the last row
            samples.append(sample)                             #or we have duplicated samples at the tri intersection

    #print('\n samples', len(samples))
    return samples



def adhesion3(NodePoint, mesh, data):
    #print("dbg pkhg L484 data = ", data)    
    targetBoundingSphere = data[0]
    minimum_dist = targetBoundingSphere[1] * bpy.context.scene.IvyMaxAdhesionDist

    #get the closest faces
    #1. naive search
    #print('\nsorting faces')
    sortetFaces = sorted(mesh.faces, key=lambda face: (face.center - NodePoint).length)

    if ((sortetFaces[0].center - NodePoint).length > minimum_dist):
        return mathutils.Vector((0,0,0))

    # now generate oversampling points for the closest faces
    samplePoints = generateSamples(sortetFaces[0:1], mesh) # generate samples for the 4 nearest faces

    sortetSample = sorted(samplePoints, key=lambda point: (NodePoint - point).length)

    #debugmesh for the sampling points
    #meshFromPoints(sortetSample)

    #print('sortetSample - all: ', len(sortetSample))

    try:
        adhesionVector = NodePoint - sortetSample[0]
        #print('NodePoint', NodePoint)
        #print('sortetSample[0]', sortetSample[0])
    except:
        adhesionVector = mathutils.Vector((0,0,0))

    #print(adhesionVector)
    return adhesionVector

def collision(ob, root, new_pos):
    prevIvy = root.ivyNodes[-1]
    old_pos = prevIvy.position
    deadlockCounter = 0
    climbing = False

    #print('Collider Object: ', ob.name)

    ray_result = ob.ray_cast(old_pos, new_pos)
    if ray_result[2] != -1: # if collides with mesh
        #print('XXXXXXXXXXXXXXCollided with Object: ', ob.name)
    
        climbing = True
        face = ob.data.faces[ray_result[2]]
        
        #project newPos to triangle plane
        #Vector3d p0 = newPos - t->norm * Vector3d::dotProduct(t->norm, newPos - t->v0->pos);
    
        projVec = mathutils.Vector((new_pos -
                                    (face.normal * face.normal.dot(new_pos - face.center))))
    
        new_pos += 2.0 * (projVec - new_pos)

    #else:
        #print('not colliding\n')
    
    collisionResult = [climbing, new_pos]
    return climbing, new_pos
    
                
def main(context, data, collider):
    seedPoint = context.scene.cursor_location  #start the Ivy at the cursor
    random.seed(bpy.context.scene.IvySeed)     #controlling the random seed
    scene = context.scene

    try:
        obj = context.active_object
        #print('context.active_object', context.active_object.name)
    except:
        obj = None

    #create main IvyObject stores all the IvyRoots.IvyNodes
    IVY = Ivy()

    #adVec = adhesion3(seedPoint, obj.data)

    #empty the roots list
    IVY.ivyRoots = []

    #set inital IvyRoot
    IVY.seed(seedPoint, parentOrder=0, primaryVector=mathutils.Vector((0,0,1)))

    #for now usercontrollable growiterations
    for i in range(bpy.context.scene.IvyIter):
        IVY.grow(data, obj)

    print('\nIvyNodes[root[0]:', len(IVY.ivyRoots[0].ivyNodes))
    print('IvyRoots:', len(IVY.ivyRoots))

    #create Curverepresentation of the Ivy
    createIvyCurves(context, IVY)


    return

class ivy_test_pkhg(bpy.types.Operator):    
    bl_idname = "ivy.ivy_test_pkhg"
    bl_label = "IVY_test"
    bl_options = {'REGISTER', 'UNDO'}

    collider = None

    @classmethod
    def poll(self, context):
        return context.active_object != None

    def execute(self, context):
        print('\n____________START_____________')
        # turn off undo
        #undo = bpy.context.user_preferences.edit.global_undo
        #bpy.context.user_preferences.edit.global_undo = False

#        props = self.properties
        data = []
        # compute boundingSphere and put into props if abs mesh is selected
        if (context.active_object
        and context.active_object.select
        and context.active_object.type == 'MESH'):
            targetBoundingSphere = computeBoundingSphere(bpy.context.selected_editable_objects)
            data.append(targetBoundingSphere)

        if self.collider == None:
            self.collider = context.active_object

        

 #       main(context, props, data, self.collider)
        main(context, data, self.collider)

        #bpy.context.user_preferences.edit.global_undo = undo
        
        return {'FINISHED'}
#########properties########3333
#Classes
def initSceneProperties(sce):
    bpy.types.Scene.IvySeed = FloatProperty(name='Seed',
                            min=0, soft_min=0,
                            default=0.5)
    sce['IvySeed'] = 0.5
    bpy.types.Scene.IvyIter = IntProperty(name='Iterations',
                            min=0, soft_min=0,
                            default=19) #89
    sce['IvyIter'] = 19
    bpy.types.Scene.IvyGravityWeight = FloatProperty(name='Gravity Weight',
                            min=0,soft_min=0,
                            max=1,soft_max=1,
                            default=0.2)
    sce['IvyGravityWeight'] = 0.2
    bpy.types.Scene.IvyPrimaryWeight = FloatProperty(name='Primary Weight',
                            min=0,soft_min=0,
                            max=1,soft_max=1,
                            default=0.5)
    sce['IvyPrimaryWeight']= 0.5
    bpy.types.Scene.IvyAdhesionWeight = FloatProperty(name='PAdhesion Weight',
                            min=0,soft_min=0,
                            max=1,soft_max=1,
                            default=0.5)
    sce['IvyAdhesionWeight'] = 0.5
    bpy.types.Scene.IvyRandomWeight = FloatProperty(name='Random Weight',
                            min=0,soft_min=0,
                            max=1,soft_max=1,
                            default=0.5)
    sce['IvyRandomWeight'] = 0.5
    bpy.types.Scene.IvyGlobScale = FloatProperty(name='Globel Scale',
                            min=0,soft_min=0,
                            default=1)
    sce['IvyGlobScale']= 1
    bpy.types.Scene.IvyMaxAdhesionDist = FloatProperty(name='MaxAdhesionDist',
                            min=0,soft_min=0,
                            default=1)    
    sce['IvyMaxAdhesionDist'] = 1
    return

initSceneProperties(bpy.context.scene)





######### new classes ##########
class OBJECT_PT_ivy_panel(bpy.types.Panel):
    bl_label = "grow an ivy"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"
#    bl_options = {'DEFAULT_CLOSED'}
    



    @classmethod
    def poll(cls, context):
        result = bpy.context.active_object.type == "MESH"
        return result


#    def draw_header(self, context):
#        layout = self.layout
#        view = context.space_data
#        layout.prop(view, "run cells script", text="")


    def draw(self,context):
#        objects = getSelected()
        sce = context.scene
        view = context.active_object
        layout = self.layout
#        box = layout.box()
#        box.operator("mesh.generate_castle")
        #box = layout.box()
#        box.label("Next step =" + str(sce.MyStepNow),icon='HAND')
        box = layout.box()
        box.label(text="set Values!")
        box.prop(sce,'IvySeed')
        box.prop(sce,'IvyIter')
        box.prop(sce,'IvyGravityWeight')
        box.prop(sce,'IvyPrimaryWeight')
        box.prop(sce,'IvyAdhesionWeight')
        box.prop(sce,'IvyRandomWeight')
        box.prop(sce,'IvyGlobScale')
        box.prop(sce,'IvyMaxAdhesionDist')
        box.operator(ivy_test_pkhg.bl_idname)
#        box.label(text="Start at")
#        box.prop(sce,'NewStart')
#        box.prop(sce,'MyEnum') 
#        box.operator("mesh.new_start")
#        box = layout.box()
#        box.label(text="Not yet implemented")
#        box.prop(sce,'MyLang')

        
###########registering#############
#    bpy.types.register(ivy_test2)
ivy_test_button2 = (lambda self, context: self.layout.operator
           (ivy_test2.bl_idname,text="ivy.ivy_test2",icon="PLUGIN"))
           
#bpy.types.VIEW3D_PT_tools_objectmode.prepend(ivy_test_button2) #just for testing
    
def register():
    bpy.utils.register_module(__name__)

def unregister():
    bpy.utils.unregister_module(__name__)


if __name__ == "__main__":
    register()    
