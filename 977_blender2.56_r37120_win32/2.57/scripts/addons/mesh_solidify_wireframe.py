#!BPY

bl_info = {
    "name": "Solidify Wireframe",
    "author": "Yorik van Havre, Alejandro Sierra",
    "description": "Turns the selected edges of a mesh into solid geometry",
    "version": (2, 0),
    "blender": (2, 5, 6),
    "category": "Mesh",
    "location": "Mesh > Solidify Wireframe",
    "warning": '',
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.5/Py/Scripts/Modeling/Solidify_Wireframe",
    "tracker_url": "https://projects.blender.org/tracker/index.php?https://projects.blender.org/tracker/index.php?func=detail&aid=21421"
    }

# ***** BEGIN GPL LICENSE BLOCK *****
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# as published by the Free Software Foundation; either version 2
# of the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See th
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software Foundation,
# Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
#
# ***** END GPL LICENCE BLOCK *****

import bpy, mathutils

def create_cube(me, v, d):
    x = v.co.x
    y = v.co.y
    z = v.co.z
    coords=[ [x-d,y-d,z-d], [x+d,y-d,z-d], [x+d,y+d,z-d], [x-d,y+d,z-d],
         [x-d,y-d,z+d], [x+d,y-d,z+d], [x+d,y+d,z+d], [x-d,y+d,z+d] ]
    for coord in coords:
        me.vertices.add(1)
        me.vertices[-1].co = mathutils.Vector(coord)

def find_intersected_face(v, face_used=-1):
    normals=[ [0,0,1], [0,0,-1], [0,1,0], [0,-1,0], [-1,0,0], [1,0,0] ] 
    dotmax = 0
    index = 0
    vn = v.normalized()
    for i in range(6):
        n = normals[i]
        a = mathutils.Vector((n[0], n[1], n[2]))
        d = -a*vn
        if d > dotmax and face_used!=i:
            dotmax=d
            index = i
    return index, dotmax

def fill_cube_face(me, index, f):
#    faces= [ [0,1,2,3], [4,7,6,5], [0,4,5,1],
#             [1,5,6,2], [2,6,7,3], [4,0,3,7] ]
    faces= [ [3,2,1,0], [4,5,6,7], [0,1,5,4],
             [7,6,2,3], [1,2,6,5], [4,7,3,0] ]       
    fdata=[ index+faces[f][0], 
              index+faces[f][1],
              index+faces[f][2],
              index+faces[f][3]]
    return fdata

def skin_edges(me, i1, i2, f1, f2):
    faces= [ [3,2,1,0], [4,5,6,7], [0,1,5,4],
             [7,6,2,3], [1,2,6,5], [4,7,3,0] ]
    fdata = [ i1+faces[f1][0], 
              i2+faces[f2][3],
              i2+faces[f2][2],
              i1+faces[f1][1]]
    fdata.extend([ i1+faces[f1][1], 
              i2+faces[f2][2],
              i2+faces[f2][1],
              i1+faces[f1][2]])
    fdata.extend([ i1+faces[f1][2], 
              i2+faces[f2][1],
              i2+faces[f2][0],
              i1+faces[f1][3]])
    fdata.extend([ i1+faces[f1][3], 
              i2+faces[f2][0],
              i2+faces[f2][3],
              i1+faces[f1][0]])
    return fdata

def create_wired_mesh(me, thick):
    node = {}

    # Create node list
    for e in me.edges:
        if e.select:
            i0 = e.key[0]
            i1 = e.key[1]
            if not i0 in node:
                node[i0] = []
            if not i1 in node:
                node[i1] = []
            f1, d = find_intersected_face(me.vertices[i1].co-me.vertices[i0].co)
    
            if (f1 % 2 == 0):
                f2 = f1 + 1
            else:
                f2 = f1 - 1

            # adjust faces in case they are repeated
            for i in range(len(node[i0])):
                if node[i0][i][1] == f1:
                    d2 = node[i0][i][3]
                    if d < d2:                      
                        f1, d = find_intersected_face(me.vertices[i1].co-me.vertices[i0].co, f1)
                    else:
                        i2 = node[i0][i][0]
                        f3, d3= find_intersected_face(me.vertices[i2].co-me.vertices[i0].co, f1)
                        node[i0][i][2] = f3
                        node[i0][i][3] = d3

            for i in range(len(node[i1])):
                if node[i1][i][1] == f2:
                    d2 = node[i1][i][3]
                    if d < d2:                      
                        f2, d = find_intersected_face(me.vertices[i0].co-me.vertices[i1].co, f2)
                    else:
                        i2 = node[i1][i][0]
                        f3, d3= find_intersected_face(me.vertices[i2].co-me.vertices[i1].co, f2)
                        node[i1][i][2] = f3
                        node[i1][i][3] = d3
                
            node[i0].append([i1, f1, f2, d])
            node[i1].append([i0, f2, f1, d])    

    me2 = bpy.data.meshes.new(me.name)

    # Create the geometry
    n_idx = {}   
    for k in node:
        v = me.vertices[k]
        index = len(me2.vertices)
        # We need to associate each node with the new geometry
        n_idx[k] = index   
        # Geometry for the nodes, each one a cube
        create_cube(me2, v, thick)
        
        
    # Skin using the new geometry 
    cfaces = []  
    for k in node:
        # Compute the face list
        f = [-1,-1,-1,-1,-1,-1] 
        n = node[k]
        
        for i in range(len(n)):
            f1 = n[i][1]
            f[f1] = i


        # Skin the nodes             
        for i in range(6):
            if f[i] == -1:
                cfaces.extend(fill_cube_face(me2, n_idx[k], i))
            else:
                k2 = n[f[i]][0]
                f1 = n[f[i]][1]
                f2 = n[f[i]][2]
                cfaces.extend(skin_edges(me2, n_idx[k], n_idx[k2], f1, f2))

    # adding faces to the mesh                
    me2.faces.add(len(cfaces) // 4)
    me2.faces.foreach_set("vertices_raw", cfaces)
    me2.update()
                
    # debug info
    #print ('vert count '+str(len(me2.vertices)))
    #print ('edge count '+str(len(me2.edges)))
    #print ('face count '+str(len(me2.faces)))

    return me2

# a class for your operator
class SolidifyWireframe(bpy.types.Operator):
    '''Turns the selected edges of a mesh into solid objects'''
    bl_idname = "mesh.solidify_wireframe"
    bl_label = "Solidify Wireframe"
    bl_options = {'REGISTER', 'UNDO'}
    thickness = bpy.props.FloatProperty(name="Thickness",
                                        description="Thickness of the skinned edges",
                                        default=0.1)
    
    def invoke(self, context, event):
        wm = context.window_manager
        return wm.invoke_props_dialog(self)
    
    def execute(self, context):
            # Get the active object
            ob_act = bpy.context.active_object
            # Check to see if it is a mesh
            if not ob_act or ob_act.type != 'MESH':
                    self.report({'ERROR'}, "No mesh selected!")
                    return {'CANCELLED'}
            # getting current edit mode
            currMode = ob_act.mode
            # switching to object mode
            bpy.ops.object.mode_set(mode='OBJECT')
            # getting mesh data
            mymesh = ob_act.data
            #getting new mesh
            newmesh = create_wired_mesh(mymesh,self.thickness) #THICK.val/2.0
            obj = bpy.data.objects.new(newmesh.name,newmesh)
            obj.location = ob_act.location
            obj.rotation_euler = ob_act.rotation_euler
            obj.scale = ob_act.scale
            bpy.context.scene.objects.link(obj)
                        
            # restoring original editmode
            bpy.ops.object.mode_set(mode=currMode)
            # returning after everything is done
            return {'FINISHED'}

# Register the operator
def solidifyWireframe_menu_func(self, context):
        self.layout.operator(SolidifyWireframe.bl_idname, text="Solidify Wireframe", icon='PLUGIN')

# Add "Solidify Wireframe" menu to the "Mesh" menu.
def register():
        bpy.utils.register_class(SolidifyWireframe)
        bpy.types.VIEW3D_MT_edit_mesh_edges.append(solidifyWireframe_menu_func)

# Remove "Solidify Wireframe" menu entry from the "Mesh" menu.
def unregister():
        bpy.utils.unregister_class(SolidifyWireframe)
        bpy.types.VIEW3D_MT_edit_mesh_edges.remove(solidifyWireframe_menu_func)

if __name__ == "__main__":
        register()