n, K = map(int, input().split())
S = list(map(int, input().split()))
S.sort()
S.reverse()
k, l = 0, 1
for i in range(n - 1, 0, -1):
    if S[i] == S[i - 1]:
        l += 1
    else:
        if S[i] + K >= S[i - 1]:
            k += l
        l = 1
print(n - k)