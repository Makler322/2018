T = int(input())

for i in range(T):
    n = int(input())
    S = list(map(int, input().split()))
    S.sort()
    S.reverse()
    k = 999999990
    B = -1
    P = 0
    for j in range(n - 1):
        if S[j] == S[j + 1]:
            if B == -1:
                B = S[j]
            else:
                if k > B/S[j]:
                    k = B/S[j]
                    P = [B, S[j]]
                B = S[j]
    print(P[0], P[0], P[1], P[1])