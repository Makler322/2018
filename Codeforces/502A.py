n = int(input())
A = sum(map(int, input().split()))
S = []
for i in range(n - 1):
    S.append(sum(map(int, input().split())))
S.sort()
S.reverse()
#print(S)
for i in range(n - 1):
    if A >= S[i]:
        print(i + 1)
        exit()
print(n)