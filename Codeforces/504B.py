n, k = map(int, input().split())

if k % 2 == 0:
    if k // 2 >= n:
        print(0)
        exit()
    l = k // 2 - 1
    if k - 1 > n:
        l -= (k - 1) - n
    print(l)
if k % 2 == 1:
    if k // 2 >= n:
        print(0)
        exit()
    l = k // 2
    if k - 1 > n:
        l -= (k - 1) - n
    print(l)