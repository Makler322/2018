n = int(raw_input())
def Factor(n):
   Ans = set()
   d = 2
   while d * d <= n:
       if n % d == 0:
           Ans.add(d)
           n //= d

       else:
           d += 1
   if n > 1:
       Ans.add(n)
   return Ans
a, b = map(int, raw_input().split())
L = Factor(a)
N = Factor(b)
#print(L, N)
S = N.union(L)
#print(S)
for i in range(n - 1):
    a, b = map(int, raw_input().split())
    L = Factor(a)
    N = Factor(b)
    P = N.union(L)
    S = P.intersection(S)
    #print(S)
    if len(S) == 0:
        print -1
        exit()
l = list(S)
print l[0]
