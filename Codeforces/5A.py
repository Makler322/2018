n, m = map(int, input().split())
for i in range(n):
    A = input()
    if 'B' in A:
        p = A.find('B') + 1
        l = A.count('B')
        print(i + 1 + (l - 1) // 2, p + (l - 1) // 2)
        exit()
