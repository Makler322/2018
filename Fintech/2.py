from fractions import gcd
a1, b1, n = map(int, input().split())
a2, b2, m = map(int, input().split())
b = n * m // gcd(n,m)
S1 = [0] * (b*b)
S2 = [0] * (b*b)
p = b // n
pl = b // m
k = 0
for j in range(p):
    for i in range(a1, b1+1):
        S1[i] = 1
    a1, b1 = a1 + n, b1 + n
for j in range(pl):
    for i in range(a2, b2+1):
        S2[i] = 1
    a2, b2 = a2 + m, b2 + m

k1, k = 0, 0
for i in range(0, 2*b):
    if S1[i] == 1 and S2[i] == 1:
        k1 += 1
    else:
        k = max(k, k1)
        k1 = 0
print(k)