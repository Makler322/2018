file = open('input.txt', mode = 'r')
n = int(file.readline())
A = list(map(int, file.readline().split()))
m = max(A)
M = min(A) - 1
k = 1
S = []
for i in A:
    if i > M:
        S.append(k)
        M = i
        k = 0
    if i <= M:
        k += 1
    if i == m:
        S.append(k)
        break
#print(S)
file2 = open('output.txt', 'w')
file2.write(str(max(S)))