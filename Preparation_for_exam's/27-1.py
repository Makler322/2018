A = input().split('.')
B = ['0','1','2','3','4','5','6','7','8','9']
F = {}
Answer = []
Flag = True
for i in A[0]:
    if i in B:
        Flag = False
        i = int(i)
        if i not in F:
          F[i] = 1
        else:
          F[i] += 1
if Flag:
    print(-1)
    exit()

for i in range(9,-1,-1):
    if i in F:
        Answer.append([str(i)]*int(F[int(B[i])]))

for i in ''.join(map(str, Answer)):
    if i in B:
        print(i, end = '')