def rotate(A,B,C):
  return (B[0]-A[0])*(C[1]-B[1])-(B[1]-A[1])*(C[0]-B[0])

def grahamscan(A): # Алгоритм Грэхема
    D = []
    n = len(A) # число точек
    P = list(range(n)) # список номеров точек
    for i in range(1,n):
        if A[P[i]][0] < A[P[0]][0]: # если P[i]-ая точка лежит левее P[0]-ой точки
            P[i], P[0] = P[0], P[i] # меняем местами номера этих точек
    for i in range(2,n): # сортировка вставкой
        j = i
        while j > 1 and (rotate(A[P[0]],A[P[j-1]],A[P[j]]) < 0):
            P[j], P[j-1] = P[j-1], P[j]
            j -= 1
    S = [P[0],P[1]] # создаем стек
    for i in range(2,n):
        while rotate(A[S[-2]],A[S[-1]],A[P[i]]) < 0:
            S.pop() # pop(S)
        S.append(P[i]) # push(S,P[i])
    for i in range(len(S)):
        D.append(A[S[i]])
    return D

def square(a, b, c):
    s = (a[0] - c[0]) * (b[1] - c[1]) + (b[0] - c[0]) * (c[1] - a[1])
    if s > 0:
        return 1
    if s < 0:
        return -1
    return 0
def intersect1 (a, b, c, d):
    return max(a, b) >= min(c, d) and max(c, d) >= min(a, b)
def intersect(a,b, c, d):
    s11 = square(a, b, c)
    s12 = square(a, b, d)
    s21 = square(c, d, a)
    s22 = square(c, d, b)
    if (s11 == 0 and s12 == 0 and s21 == 0 and s22 == 0):
        return intersect1(a[0], b[0], c[0], d[0]) and intersect1(a[1], b[1], c[1], d[1])
    return (s11*s12 <= 0) and (s21*s22 <= 0)

def peresechenie(x1_1, y1_1, x1_2, y1_2, x2_1, y2_1, x2_2, y2_2):
    A = [x1_1, y1_1]
    B = [x1_2, y1_2]
    C = [x2_1, y2_1]
    D = [x2_2, y2_2]
    if intersect(A, B, C, D):
        return True
    else:
        return False


n = int(input())
Krasivo = []
Urod = []
for i in range(n):
    K = list(map(int, input().split()))
    if K[2] == 1:
        Krasivo.append([K[0], K[1]])
    else:
        Urod.append([K[0], K[1]])
#print(Krasivo)
#print(Urod)
if len(Krasivo) > 2:
    K_MVO = grahamscan(Krasivo)
else:
    K_MVO = Krasivo

if len(Urod) > 2:
    U_MVO = grahamscan(Urod)
else:
    U_MVO = Urod

if len(Urod) == 0 or len(Krasivo) == 0 or (len(Urod) == 1 and len(Krasivo) == 1):
    print('Yes')
    exit()

if len(U_MVO) == 1 and len(K_MVO) == 2:
    if rotate(U_MVO[0], K_MVO[0], K_MVO[1]) == 0 and (min(K_MVO[0][0], K_MVO[1][0]) <= U_MVO[0][0] <= max(K_MVO[0][0], K_MVO[1][0])) and (min(K_MVO[0][1], K_MVO[1][1]) <= U_MVO[0][1] <= max(K_MVO[0][1], K_MVO[1][1])):
        print('No')
    else:
        print('Yes')
    exit()
if len(U_MVO) == 2 and len(K_MVO) == 1:
    if rotate(U_MVO[0], U_MVO[1], K_MVO[0]) == 0 and (min(U_MVO[0][0], U_MVO[1][0]) <= K_MVO[0][0] <= max(U_MVO[0][0], U_MVO[1][0])) and (min(U_MVO[0][1], U_MVO[1][1]) <= K_MVO[0][1] <= max(U_MVO[0][1], U_MVO[1][1])):
        print('No')
    else:
        print('Yes')
    exit()

if len(U_MVO) == 1:
    if K_MVO == grahamscan(K_MVO+U_MVO):
        print('No')
    else:
        for i in range(len(K_MVO)):
            if rotate(K_MVO[i], U_MVO[0], K_MVO[(i+1) % len(K_MVO)]) == 0 and (min(K_MVO[i][0], K_MVO[(i+1) % len(K_MVO)][0]) <= U_MVO[0][0] <= max(K_MVO[i][0], K_MVO[(i+1) % len(K_MVO)][0])) and (min(K_MVO[i][1], K_MVO[(i+1) % len(K_MVO)][1]) <= U_MVO[0][1] <= max(K_MVO[i][1], K_MVO[(i+1) % len(K_MVO)][1])):
                print('No')
                exit()
        print('Yes')
    exit()

if len(K_MVO) == 1:
    if U_MVO == grahamscan(U_MVO+K_MVO):
        print('No')
    else:
        for i in range(len(U_MVO)):
            if rotate(U_MVO[i], K_MVO[0], U_MVO[(i+1) % len(U_MVO)]) == 0 and (min(U_MVO[i][0], U_MVO[(i+1) % len(U_MVO)][0]) <= K_MVO[0][0] <= max(U_MVO[i][0], U_MVO[(i+1) % len(U_MVO)][0])) and (min(U_MVO[i][1], U_MVO[(i+1) % len(U_MVO)][1]) <= K_MVO[0][1] <= max(U_MVO[i][1], U_MVO[(i+1) % len(U_MVO)][1])):
                print('No')
                exit()
        print('Yes')
    exit()

Flag = True
for i in range(len(U_MVO)):
    for j in range(len(K_MVO)):
        if peresechenie(U_MVO[i][0],U_MVO[i][1], U_MVO[(i+1) % len(U_MVO)][0],U_MVO[(i+1) % len(U_MVO)][1], K_MVO[j][0],K_MVO[j][1], K_MVO[(j+1) % len(K_MVO)][0],K_MVO[(j+1) % len(K_MVO)][1]):
            Flag = False

if grahamscan(K_MVO + [U_MVO[0]]) == grahamscan(K_MVO) and Flag:
    Flag = False
if grahamscan(U_MVO + [K_MVO[0]]) == grahamscan(U_MVO) and Flag:
    Flag = False

if Flag:
    print('Yes')
else:
    print('No')
'''
12
0 2 1
0 5 1
1 4 1
2 1 1
2 5 1
2 6 1
3 3 1
3 5 2
4 0 2
4 4 2
5 6 2
7 3 2
'''