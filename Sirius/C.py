def rotate(A,B,C):
  return (B[0]-A[0])*(C[1]-B[1])-(B[1]-A[1])*(C[0]-B[0])
def peresechenie(x1_1, y1_1, x1_2, y1_2, x2_1, y2_1, x2_2, y2_2):
    # составляем формулы двух прямых
    A1 = y1_1 - y1_2
    B1 = x1_2 - x1_1
    C1 = x1_1 * y1_2 - x1_2 * y1_1
    A2 = y2_1 - y2_2
    B2 = x2_2 - x2_1
    C2 = x2_1 * y2_2 - x2_2 * y2_1
    if A1 == 0 and A2 == 0:
        if B1 == B2 and C1 == C2:
            return True
        else:
            return False

    elif A1 == 0 and B1 == 0:
        if A2 * y1_1 + B2 * x1_1 + C2 == 0:
            return False
        else:
            return True
    elif A1 == 0:
        y = C1 / B1
        x = (-C2 - B2 * y) / A2

    elif B1 * A2 - B2 * A1 != 0:
        y = (C2 * A1 - C1 * A2) / (B1 * A2 - B2 * A1)
        x = (-C1 - B1 * y) / A1
    # случай деления на ноль, то есть параллельность
    else:
        if rotate([x1_1, y1_1], [x1_2, y1_2], [x2_1, y2_1]) == 0:
            if ((max(x1_1, x1_2) <= min(x2_1, x2_2)) and (max(y1_1, y1_2) <= min(y2_1, y2_2))) or \
                    ((max(x2_1, x2_2) <= min(x1_1, x1_2)) and (max(y2_1, y2_2) <= min(y1_1, y1_2))):
                return False
            if ((min(x1_1, x1_2) <= min(x2_1, x2_2)) and (max(x2_1, x2_2) <= max(x1_1, x1_2))) and ((min(y1_1, y1_2) <= min(y2_1, y2_2)) and (max(y2_1, y2_2) <= max(y1_1, y1_2))) or \
                ((min(x2_1, x2_2) <= min(x1_1, x1_2)) and (max(x1_1, x1_2) <= max(x2_1, x2_2))) and ((min(y2_1, y2_2) <= min(y1_1, y1_2)) and (max(y1_1, y1_2) <= max(y2_1, y2_2))):
                return True
            if ((min(x1_1, x1_2) <= min(x2_1, x2_2)) and (max(x2_1, x2_2) >= max(x1_1, x1_2))) and ((min(y1_1, y1_2) <= min(y2_1, y2_2)) and (max(y2_1, y2_2) >= max(y1_1, y1_2))) or \
                ((min(x2_1, x2_2) <= min(x1_1, x1_2)) and (max(x1_1, x1_2) >= max(x2_1, x2_2))) and ((min(y2_1, y2_2) <= min(y1_1, y1_2)) and (max(y1_1, y1_2) >= max(y2_1, y2_2))):
                return True
        return False
    if min(x1_1, x1_2) <= x <= max(x1_1, x1_2) and min(y1_1, y1_2) <= y <= max(y1_1, y1_2) and \
                            min(x2_1, x2_2) <= x <= max(x2_1, x2_2) and min(y2_1, y2_2) <= y <= max(y2_1, y2_2):
        return True
    else:
        return False
x1_1, y1_1, x1_2, y1_2 = map(int, input().split())
x2_1, y2_1, x2_2, y2_2 = map(int, input().split())
if peresechenie(x1_1, y1_1, x1_2, y1_2, x2_1, y2_1, x2_2, y2_2):
    print('YES')
else:
    print('NO')