import collections as coll
n, m = map(int,input().split())  # 4 3
k, l = map(int,input().split())
x1_start, y1_start, x1_end, y1_end = map(int,input().split()) #input data

def check(x, y):
    global n, m
    if x < 0 or x >= n or y < 0 or y >= m:
        return False
    return Used[y][x]

Used = [[True for i in range(n)] for i in range(m)]
Pre = [[[False, False] for i in range(n)] for i in range(m)]

deck = coll.deque()
deck.append([x1_start, y1_start])

for i in range(m):
    print(' '.join(map(str, Used[i])))
print()
for i in range(m):
    S = list(map(int, input().split()))
    for j in range(n):
        if S[j] == 1:
            Used[i][j] = False

for i in range(m):
    print(' '.join(map(str, Used[i])))
print()
dx1 = [k, k, l, l, -k, -k, -l, -l]
dy1 = [l, -l, k, -k, l, -l, k, -k]

x0_1, y0_1 = deck[0]

while len(deck) != 0 and [x0_1, y0_1] != [x1_end, y1_end]:
    x0_1, y0_1 = deck[0]
    for i1, j1 in zip(dx1, dy1):
        if check(x0_1 + i1, y0_1 + j1):
            deck.append([x0_1 + i1, y0_1 + j1])
            Used[y0_1 + j1][x0_1 + i1] = False
            Pre[y0_1 + j1][x0_1 + i1] = [x0_1, y0_1]
    deck.popleft()

for i in range(m):
    print(' '.join(map(str, Used[i])))
print()
for i in range(m):
    print(' '.join(map(str, Pre[i])))
#print(Used)
#print(Pre)
print(x0_1, y0_1)
if [x0_1, y0_1] != [x1_end, y1_end]:
    print(-1)
    exit(0)

answer = []
Itog = [x1_end, y1_end]
while Itog != [x1_start, y1_start]:
    answer.append(Itog)
    x1, y1 = Itog
    Itog = Pre[y1][x1]

print(len(answer))